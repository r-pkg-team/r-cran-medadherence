Source: r-cran-medadherence
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-medadherence
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-medadherence.git
Homepage: https://cran.r-project.org/src/contrib/Archive/medAdherence/
Rules-Requires-Root: no

Package: r-cran-medadherence
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R Medication Adherence: Commonly Used Definitions
 Adherence is defined as "the extent to which a person's behavior
 coincides with medical or health advice", which is very important, for
 both clinical researchers and physicians, to identify the treatment
 effect of a specific medication(s).
 .
 A variety of measures have been developed to calculate the medication
 adherence. Definitions and methods to address adherence differ greatly
 in public literature. Choosing which definition should be determined by
 overall study goals.  This package provides the functions to calculate
 medication adherence based on commonly used definitions.
